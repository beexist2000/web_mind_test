export default {
    GET_CURRENT_LEVEL: 'mind-test/getCurrentLevel',
    GET_MIND_TESTS: 'mind-test/getMindTests',
    GET_SELECTED_TESTS: 'mind-test/getSelectedTests',
    GET_MIND_TEST_RESULTS: 'mind-test/getMindTestResults',

    FETCH_CURRENT_LEVEL: 'mind-test/fetchCurrentLevel',
    FETCH_SELECTED_TESTS: 'mind-test/fetchSelectedTests',
    FETCH_GAME_RESET: 'mind-test/fetchGameReset'
}
