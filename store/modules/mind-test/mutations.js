import Constants from './constants'
import state from './state'

export default {
    [Constants.FETCH_CURRENT_LEVEL]: (state, payload) => {
        state.currentLevel = payload
    },
    [Constants.FETCH_SELECTED_TESTS]: (state, payload) => {
        let tempSelectedTests = state.selectedTests
        tempSelectedTests.push(payload)

        state.selectedTests = tempSelectedTests
    },
    [Constants.FETCH_GAME_RESET]: (state) => {
        state.currentLevel = 0
        state.selectedTests = []
    },
}
